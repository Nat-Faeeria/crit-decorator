# Crits Decorator for Foundry VTT

## Goal

This module allows you to define function that will detect critical successes or critical failures,
and actions that will happen in those cases.

## How To Use

  1. Install the module
  2. Activate the module
  3. In your settings, you will find the followings fields:
  
  - `Should apply` : this allows you to set a condition to determine when to apply your quest for
    critical rolls. 
    
    **This must be a Javascript function returning a Boolean!**

    Default is `(roll)=>roll.terms[0].faces==20`, which checks if you are rolling D20's. You can 
    easily change that for D100 or other types by changing the `20` by something else (e.g. `100`)
    or change the function entirely. 

- `Enable Critical Successes ?` : checking this box will enable a verification of each check 
complying with the `Should apply` condition to see if it is a critical success.

- `Critical Success condition` : the condition to determine if the roll is a critical success or not.

    **This must be a Javascript function returning a Boolean! This function must accept two arguments!**

    The first argument you get is the naked roll, the second argument is the total result of the roll.

    Default is `(result, total)=>result==20`, detecting crits only on natural 20s.

- `Critical Success action` : the action taken if the roll is a crit success.

    **This must be a Javascript function! This function must accept one argument only!**

    The argument you get is the result of the roll. 

    Default is 
    ```
    (total)=> { 
        let chatData = { 
            user: game.user.id, 
            content: `It is a critical success`, 
            speaker: ChatMessage.getSpeaker()
        };
        ChatMessage.create(chatData, {});
    }
    ```
    as a one-liner, displaying a simple chat message saying that you got a critical success.

- `Enable Critical Failures ?` : checking this box will enable a verification of each check 
complying with the `Should apply` condition to see if it is a critical failure.

- `Critical Failure condition` : the condition to determine if the roll is a critical failure or not.

    **This must be a Javascript function returning a Boolean ! This function must accept two arguments!**

    The first argument you get is the naked roll, the second argument is the total result of the roll.

    Default is `(result, total)=>result==1`, detecting crits only on natural 1s.

- `Critical Failure action` : the action taken if the roll is a crit failure.

    **This must be a Javascript function! This function must accept one argument only!**

    The argument you get is the result of the roll. 

    Default is 
    ```
    (total)=> { 
        let chatData = { 
            user: game.user.id, 
            content: `It is a critical failure`, 
            speaker: ChatMessage.getSpeaker()
        };
        ChatMessage.create(chatData, {});
    }
    ```
    as a one-liner, displaying a simple chat message saying that you got a critical failure.

## Security

Those settings are world-scoped. Do not give access to them to Players ! This module is based
on functions being evaluated then executed. Everything can happen. Be careful.

I am not liable if you mess your Foundry instance with a bad function (it should be hard to
do, but probably possible)

## Attribution and License

This module is heavily based on [**Dice So Nice**](https://gitlab.com/riccisi/foundryvtt-dice-so-nice) 
by **Simone Ricciardi** and **JDW**

It is licensed under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development](https://foundryvtt.com/article/license/).

