Hooks.once('init', () => {

    game.settings.register("crits-decorator", "should_apply", {
        name: "CRITSDECORATOR.should_apply",
        hint: "CRITSDECORATOR.should_apply_hint",
        scope: "world",
        default: "(roll)=>roll.terms[0].faces==20",
        type: String,
        config: true
    });

    game.settings.register("crits-decorator", "success_enabled", {
        name: "CRITSDECORATOR.success_enabled",
        hint: "CRITSDECORATOR.success_enabled_hint",
        scope: "world",
        type: Boolean,
        config: true
    });

    game.settings.register("crits-decorator", "success_condition", {
        name: "CRITSDECORATOR.success_condition",
        hint: "CRITSDECORATOR.success_condition_hint",
        scope: "world",
        default: "(result, total)=>result==20",
        type: String,
        config: true
    });

    game.settings.register("crits-decorator", "success_fct", {
        name: "CRITSDECORATOR.success_fct",
        hint: "CRITSDECORATOR.success_fct_hint",
        scope: "world",
        default: "(total)=> { let chatData = { user: game.user.id, content: `It is a critical success`, speaker: ChatMessage.getSpeaker()};ChatMessage.create(chatData, {});}",
        type: String,
        config: true
    });

    game.settings.register("crits-decorator", "failure_enabled", {
        name: "CRITSDECORATOR.failure_enabled",
        hint: "CRITSDECORATOR.failure_enabled_hint",
        scope: "world",
        type: Boolean,
        config: true
    });

    game.settings.register("crits-decorator", "failure_condition", {
        name: "CRITSDECORATOR.failure_condition",
        hint: "CRITSDECORATOR.failure_condition_hint",
        scope: "world",
        default: "(result, total)=>result==1",
        type: String,
        config: true
    });

    game.settings.register("crits-decorator", "failure_fct", {
        name: "CRITSDECORATOR.failure_fct",
        hint: "CRITSDECORATOR.failure_fct_hint",
        scope: "world",
        default: "(total)=> { let chatData = { user: game.user.id, content: `It is a critical failure`, speaker: ChatMessage.getSpeaker()};ChatMessage.create(chatData, {});}",
        type: String,
        config: true
    });

});



/**
 * Intercepts all roll-type messages hiding the content until the animation is finished
 */
 Hooks.on('createChatMessage', (chatMessage) => {
    let hasInlineRoll = chatMessage.data.content.indexOf('inline-roll') !== -1;
    if ((!chatMessage.isRoll && !hasInlineRoll) ){
        return;
    }
    const shouldApply = eval(game.settings.get("crits-decorator", "should_apply"))
    let roll = -1;
    
    if(game.modules.get("betterrolls5e") && game.modules.get("betterrolls5e").active) {
        roll = chatMessage.data.flags.betterrolls5e.entries.filter( t => t.type === "multiroll")[0].entries[0].roll;
    }
    else {
        roll = chatMessage.roll;
    } 

    if (shouldApply(roll)) {
        let success_condition = (x)=>false, success = (x) => x
        let failure_condition = (x)=>false, failure = (x) => x
    
        if (game.settings.get("crits-decorator", "success_enabled")) {
            success_condition = eval(game.settings.get("crits-decorator", "success_condition"))
            success = eval(game.settings.get("crits-decorator", "success_fct"))
        }
        if (game.settings.get("crits-decorator", "failure_enabled")) {
            failure_condition = eval(game.settings.get("crits-decorator", "failure_condition"))
            failure = eval(game.settings.get("crits-decorator", "failure_fct"))
        }
        let diceResult = roll.results[0]
        let total = roll.total
        if (success_condition(diceResult, total)) {
            Hooks.once("diceSoNiceRollComplete", (total)=>success(total))
        } else if (failure_condition(diceResult, total)) {
            Hooks.once("diceSoNiceRollComplete", (total)=>failure(total))
        }
        return;
    } else { return; }
});
